<?php
/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('ngp_left_span'))) {
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(
    'ngp_left_span' => 4,
    'ngp_left_prepend' => 1,
    'ngp_left_append' => 1,
    'ngp_left_top' => 1,
    'ngp_left_bottom' => 1,
    
    'ngp_right_span' => 5,
    'ngp_right_prepend' => 1,
    'ngp_right_append' => 1,
    'ngp_right_top' => 1,
    'ngp_right_bottom' => 1,
    
    'ngp_center_prepend' => 1,
    'ngp_center_append' => 1,
    'ngp_center_top' => 1,
    'ngp_center_bottom' => 1,
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

/**
* Override or insert PHPTemplate variables into the templates.
*/
function clean_preprocess_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}