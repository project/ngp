<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
  	<title><?php print $head_title ?></title>
  	<meta http-equiv="content-language" content="<?php print $language->language ?>" />
  	<?php print $meta; ?>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 7]>
      <link rel="stylesheet" href="<?php print drupal_get_path('theme', 'blueprint'); ?>/blueprint/blueprint/ie.css" type="text/css" media="screen, projection">
    	<? /* <link href="<?php print $path; ?>fix-ie.css" rel="stylesheet"  type="text/css"  media="screen, projection" /> */ ?>
    <![endif]-->  
    <!--[if lte IE 6]>
    	<? /* <link href="<?php print $path; ?>fix-ie6.css" rel="stylesheet"  type="text/css"  media="screen, projection" /> */ ?>
    <![endif]-->  
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="background-header" class="background clearfix">
      <div id="header" class="container clearfix">
        <?php if ($header): ?><div class="blocks"><?php print $header; ?></div><?php endif ?>
        <div id="logo-floater"><?php print $site_header; ?></div>
      </div>
    </div>
    <div id="background-navigation" class="background clearfix">
      <div id="navigation" class="container clearfix">
        <?php if (isset($primary_links)) print menu_tree('primary-links'); ?>
      </div>
    </div>
    <div id="background-body" class="background clearfix">
      <div id="content" class="container clearfix">
        <?php if ($top): ?><div id="blocks-top" class="region clearfix"><?php print $top; ?></div><?php endif ?>
        <?php if ($left): ?><div id="blocks-left" class="region clearfix <?php print $left_classes; ?>"><?php print $left; ?></div><?php endif ?>
        
        <div id="body" class="<?php print $center_classes; ?> clearfix">
          <?php
            if ($context_links) print '<div class="context-links">'. $context_links .'</div>';
            if ($title != '') print '<h2 class="page-title">'. $title .'</h2>';
            if ($tabs != '') print '<div class="tabs">'. $tabs .'</div>';
            if ($messages != '') print '<div id="messages">'. $messages .'</div>';
            if ($content_top) print $content_top;
            
            print $help; // Drupal already wraps this one in a class      
            
            print $content;
            print $feed_icons;
            
            if ($breadcrumb != '') print $breadcrumb;
          ?>
        </div>
        
        <?php if ($right): ?><div id="blocks-right" class="region clearfix <?php print $right_classes; ?>"><?php print $right; ?></div><?php endif ?>
      </div>
    </div>
    <div id="background-footer" class="background clearfix">
      <div id="footer" class="container clearfix prepend-top append-bottom">
        <?php if (isset($secondary_links)) print ngp_footer_links($secondary_links, array('id' => 'secondary', 'class' => 'links')); ?>
        <?php if ($footer) print $footer; ?>
        <?php if ($footer_message) print '<div id="footer-message">' . $footer_message . '</div>'; ?>
        <div id="poweredby"><img src="<?php print $path; ?>images/poweredbyngp.gif" alt="Powered by NGP Software" border="0" /> Powered by <a href="http://www.ngpsoftware.com">NGP Software</a>.</div>
      </div>
    </div>
    <?php print $scripts ?>
    <?php print $closure; ?>
  </body>
</html>
