<?php

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function glass_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'ngp_left_span' => 4,
    'ngp_left_prepend' => 1,
    'ngp_left_append' => 1,
    'ngp_left_top' => 1,
    'ngp_left_bottom' => 1,
    
    'ngp_right_span' => 5,
    'ngp_right_prepend' => 1,
    'ngp_right_append' => 1,
    'ngp_right_top' => 1,
    'ngp_right_bottom' => 1,
    
    'ngp_center_prepend' => 1,
    'ngp_center_append' => 1,
    'ngp_center_top' => 1,
    'ngp_center_bottom' => 1,
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API
  $form['left'] = array(
    '#type' => 'fieldset',
    '#title' => t('Left Column'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['left']['ngp_left_span'] = array(
    '#type' => 'textfield',
    '#title' => t('Span'),
    '#default_value' => $settings['ngp_left_span'],
    '#size' => 3,
  );
  $form['left']['ngp_left_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('Prepend'),
    '#default_value' => $settings['ngp_left_prepend'],
    '#size' => 3,
  );
  $form['left']['ngp_left_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Append'),
    '#default_value' => $settings['ngp_left_append'],
    '#size' => 3,
  );
  $form['left']['ngp_left_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prepend top'),
    '#default_value' => $settings['ngp_left_top'],
    '#return_value' => 1,
    '#size' => 3,
  );
  $form['left']['ngp_left_bottom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append bottom'),
    '#default_value' => $settings['ngp_left_bottom'],
    '#return_value' => 1,
    '#size' => 3,
  );
  
  $form['right'] = array(
    '#type' => 'fieldset',
    '#title' => t('Right Column'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['right']['ngp_right_span'] = array(
    '#type' => 'textfield',
    '#title' => t('Span'),
    '#default_value' => $settings['ngp_right_span'],
    '#size' => 3,
  );
  $form['right']['ngp_right_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('Prepend'),
    '#default_value' => $settings['ngp_right_prepend'],
    '#size' => 3,
  );
  $form['right']['ngp_right_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Append'),
    '#default_value' => $settings['ngp_right_append'],
    '#size' => 3,
  );
  $form['right']['ngp_right_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prepend top'),
    '#default_value' => $settings['ngp_right_top'],
    '#return_value' => 1,
    '#size' => 3,
  );
  $form['right']['ngp_right_bottom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append bottom'),
    '#default_value' => $settings['ngp_right_bottom'],
    '#return_value' => 1,
    '#size' => 3,
  );
  
  $form['center'] = array(
    '#type' => 'fieldset',
    '#title' => t('Center Column'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['center']['ngp_center_prepend'] = array(
    '#type' => 'textfield',
    '#title' => t('Prepend'),
    '#default_value' => $settings['ngp_center_prepend'],
    '#size' => 3,
  );
  $form['center']['ngp_center_append'] = array(
    '#type' => 'textfield',
    '#title' => t('Append'),
    '#default_value' => $settings['ngp_center_append'],
    '#size' => 3,
  );
  $form['center']['ngp_center_top'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prepend top'),
    '#default_value' => $settings['ngp_center_top'],
    '#return_value' => 1,
    '#size' => 3,
  );
  $form['center']['ngp_center_bottom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Append bottom'),
    '#default_value' => $settings['ngp_center_bottom'],
    '#return_value' => 1,
    '#size' => 3,
  );

  // Return the additional form widgets
  return $form;
}