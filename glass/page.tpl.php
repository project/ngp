<?php // $Id$ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
  <head>
  	<title><?php print $head_title ?></title>
  	<meta http-equiv="content-language" content="<?php print $language->language ?>" />
  	<?php print $meta; ?>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 7]>
      <link rel="stylesheet" href="<?php print drupal_get_path('theme', 'blueprint'); ?>/blueprint/blueprint/ie.css" type="text/css" media="screen, projection">
    	<link href="<?php print $path; ?>fix-ie.css" rel="stylesheet"  type="text/css"  media="screen, projection" />
    <![endif]-->  
    <!--[if lte IE 6]>
    	<link href="<?php print $path; ?>fix-ie6.css" rel="stylesheet"  type="text/css"  media="screen, projection" />
    <![endif]-->  
  </head>
  <body class="glass <?php print $body_classes; ?>">
    <div class="background background-header">
      <div id="header" class="container clearfix">
        <?php if ($header): ?>
          <div class="blocks"><?php print $header; ?></div>
        <?php endif ?>
        <div id="logo-floater">
        <?php
          // Prepare header
          $logo_class = ($logo && !theme_get_setting('default_logo')) ? ' logo' : '';
          if ($site_name) $site_html = '<span class="title'. $logo_class .'">'. check_plain($site_name) .'</span>';
          if ($site_slogan) $site_html .= '<span class="slogan'. $logo_class .'">'. check_plain($site_slogan) .'</span>';

          if ($logo || $site_name) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. check_plain($site_name) .'">';
            if ($logo && !theme_get_setting('default_logo')) {
              print '<img src="'. check_url($logo) .'" alt="'. check_plain($site_name) .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
        </div>
      </div>
    </div>
    <div class="background background-navigation">
      <div class="container" id="navigation">
        <?php if (isset($primary_links)) print menu_tree('primary-links'); ?>
      </div>
    </div>
    <div class="background background-body">
      <div class="container clearfix" id="content">
        <?php if ($left): ?><div class="background background-left clearfix" style="background-position:<?php print $left_background; ?>px 0;"><?php endif ?>
        <?php if ($right): ?><div class="background background-right clearfix" style="background-position:<?php print $right_background; ?>px 0;"><?php endif ?>
        <?php if ($top): ?>
          <div id="blocks-top" class="region"><?php print $top; ?></div>
        <?php endif ?>
      
        <?php if ($left): ?>
          <div id="blocks-left" class="region <?php print $left_classes; ?>"><?php print $left; ?></div>
        <?php endif ?>
        
        <div id="body" class="<?php print $center_classes; ?>">
          <?php
            if ($context_links) { 
              print '<div class="context-links">'. $context_links .'</div>';
            }
            
            if ($title != '') {
              print '<h2 class="page-title">'. $title .'</h2>';
            }
            
            if ($tabs != '') {
              print '<div class="tabs">'. $tabs .'</div>';
            }
            
            if ($messages != '') {
              print '<div id="messages">'. $messages .'</div>';
            }
            
            if ($content_top) {
              print $content_top;
            }
            
            print $help; // Drupal already wraps this one in a class      
            
            print $content;
            print $feed_icons;
            
            if ($breadcrumb != '') {
              print $breadcrumb;
            }
          ?>
        </div>
        
        <?php if ($right): ?>
          <div id="blocks-right" class="region <?php print $right_classes; ?>"><?php print $right; ?></div>
        <?php endif ?>
        
        <?php if ($left): ?></div><?php endif ?>
        <?php if ($right): ?></div><?php endif ?>
      </div>
    </div>
    <div class="background background-footer">
      <div id="footer" class="container clear prepend-top append-bottom">
        <?php if (isset($secondary_links)) : ?>
          <?php print ngp_footer_links($secondary_links, array('id' => 'secondary', 'class' => 'links')) ?>
        <?php endif; ?>
        <?php if ($footer): ?>
          <?php print $footer; ?>
        <?php endif; ?>
        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
        <div class="clear"></div>
        <div id="poweredby"><img src="<?php print $path; ?>images/poweredbyngp.gif" alt="Powered by NGP Software" border="0" /> Powered by <a href="http://www.ngpsoftware.com">NGP Software</a>.</div>
      </div>
    </div>
    <div class="clear"></div>
    <?php print $scripts ?>
    <?php print $closure; ?>
  </body>
</html>
