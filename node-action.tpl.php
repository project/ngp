<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
<?php
  //dsm(get_defined_vars());
	$link = (!empty($field_link[0]['url'])) ? $field_link[0]['url'] : 'node/'.$node->nid;
?>
<?php if (!empty($field_image_home) && !empty($field_image_home[0]) && !empty($field_image_home[0]['view'])): ?>
<div class="field-image"><?php
  $imagetag = theme('imagefield_image', $field_image_home[0], $field_image_home[0]['data']['alt'], $field_image_home[0]['data']['title']);
  $class = 'imagefield imagefield-nodelink imagefield-'. $element['#field_name'];
  print l($imagetag, $link, array('attributes' => array('class' => $class), 'html' => TRUE));
?></div>
<div class="node-body">
<?php endif; ?>

  <?php
    if ($page == 0) {
			print '<h2 class="node-title">';
			print l($node->title, $link);
			print '</h2>';
    }
  ?>

  <div class="content clear-block">
    <?php print $picture ?>
    <?php print $content ?>
  </div>

  <?php if ($terms && ($node->type != 'page')): ?>
    <div class="terms"><?php print $terms ?></div>
  <?php endif;?>

<?php
  if ($links) {
    print '<div class="node-links">'. $links .'</div>';
  }
?>

<?php if (!empty($field_image_home) && !empty($field_image_home[0]) && !empty($field_image_home[0]['view'])): ?>
</div>
<?php endif; ?>

</div>