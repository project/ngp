<?php

$info = array(

  // Pre-defined color schemes.
  // base, link, top, bottom, text
  'schemes' => array(
    '#003b83,#bf0000,#0b3f73,#80a0c0,#000000' => t('NGP Blue (Default)'),
    '#464849,#5d6779,#2a2b2d,#464849,#494949' => t('Ash'),
    '#55c0e2,#007e94,#085360,#55c0e2,#696969' => t('Aquamarine'),
    '#d5b048,#971702,#331900,#d5b048,#494949' => t('Belgian Chocolate'),
    '#3f3f3f,#6598cb,#6598cb,#3f3f3f,#000000' => t('Bluemarine'),
    '#d0cb9a,#e6fb2d,#efde01,#d0cb9a,#494949' => t('Citrus Blast'),
    '#0f005c,#1a1575,#4d91ff,#0f005c,#000000' => t('Cold Day'),
    '#c9c497,#7be000,#03961e,#c9c497,#494949' => t('Greenbeam'),
    '#ffe23d,#a30f42,#fc6d1d,#ffe23d,#494949' => t('Mediterrano'),
    '#788597,#d4d4d4,#a9adbc,#788597,#707070' => t('Mercury'),
    '#5b5fa9,#9fa8d5,#0a2352,#5b5fa9,#494949' => t('Nocturnal'),
    '#7db323,#7db323,#b5d52a,#7db323,#191a19' => t('Olivia'),
    '#12020b,#f41063,#f391c6,#12020b,#898080' => t('Pink Plastic'),
    '#b7a0ba,#f21107,#a1443a,#b7a0ba,#515d52' => t('Shiny Tomato'),
    '#18583d,#52bf90,#34775a,#18583d,#2d2d2d' => t('Teal Top'),
  ),

  // Images to copy over.
  'copy' => array(
    'images/poweredbyngp.gif',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'hope.css',
  ),

  // Coordinates of gradient (x, y, width, height).
  //'gradient' => array(0,0,980,500),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0,0,1000,1000),
    'link' => array(665,159,309,801),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/action.png'  => array(25,159,640,326),
    'images/navigation.png'  => array(20,104,960,55),
    'images/page.png'  => array(20,960,960,10),
    'images/sidebar.png'  => array(665,159,310,801),
    'images/footer.png'  => array(20,970,960,30),

    'screenshot.png'                       => array(0, 37, 400, 240),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
