<?php

/**
* Override or insert PHPTemplate variables into the templates.
*/
function hope_preprocess_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

function hope_color() {
  $color = array(
	  'replacement methods' => array(
	    'shift' => array('base', 'link', 'text'),
	    'tag' => TRUE,
	  ),
	  'fields' => array('base', 'link', 'text', 'sidebar top', 'sidebar bottom', 'action'),
	  
    // Pre-defined color schemes.
    'premade schemes' => array(
      'NGP Blue (Default)' => array('base' => '#003b84', 'link' => '#003b84', 'text' => '#000000', 'sidebar top' => '#bf0000', 'sidebar bottom' => '#bf0000', 'action' => '#184d8f'),
      'Obama'              => array('base' => '#01245C', 'link' => '#2575AD', 'text' => '#424242', 'sidebar top' => '#01245C', 'sidebar bottom' => '#666666', 'action' => '#01245C'),
    ),
    
	  'reference scheme' => 'NGP Blue (Default)',
	  'default scheme' => 'NGP Blue (Default)',
  
    // CSS files (excluding @import) to rewrite with new color scheme.
    'stylesheets' => array(
      'hope.css',
    ),
	  
	  'blend target' => '#ffffff',
	  
	  'images' => array(
	    'header' => array(
    	  'file' => 'color/base.png',
    	  'fill' => array(
          array('type' => 'solid', 'x' => 25, 'y' => 159, 'width' => 640, 'height' => 326, 'colors' => array('action')),
          array('type' => 'solid', 'x' => 20, 'y' => 104, 'width' => 960, 'height' => 55, 'colors' => array('base')),
          array('type' => 'solid', 'x' => 20, 'y' => 960, 'width' => 960, 'height' => 10, 'colors' => array('base')),
          array('type' => 'solid', 'x' => 665, 'y' => 159, 'width' => 310, 'height' => 326, 'colors' => array('sidebar top')),
          array('type' => 'solid', 'x' => 665, 'y' => 485, 'width' => 310, 'height' => 475, 'colors' => array('sidebar bottom')),
          array('type' => 'solid', 'x' => 20, 'y' => 970, 'width' => 960, 'height' => 30, 'colors' => array('base')),
    	  ),
        'slices' => array(
          'images/action.png'         => array('x' => 25, 'y' => 159, 'width' => 640, 'height' => 326),
          'images/navigation.png'     => array('x' => 20, 'y' => 104, 'width' => 960, 'height' => 55),
          'images/page.png'           => array('x' => 20, 'y' => 960, 'width' => 960, 'height' => 10),
          'images/sidebar_top.png'    => array('x' => 665, 'y' => 159, 'width' => 310, 'height' => 326),
          'images/sidebar_bottom.png' => array('x' => 665, 'y' => 485, 'width' => 310, 'height' => 475),
          'images/footer.png'         => array('x' => 20, 'y' => 970, 'width' => 960, 'height' => 30),
        ),
    	),
    ),
  );

  return $color;
}