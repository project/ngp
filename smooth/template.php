<?php

/**
* Override or insert PHPTemplate variables into the templates.
*/
function smooth_preprocess_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
  //dsm($vars['styles']);
}

/*function smooth_node_submitted($node) {
  if ($node->readmore) {
    $format = '<\s\t\ro\n\g>M</\s\t\ro\n\g><\e\m>j</\e\m>';
  }
  else {
    $format = 'F j, Y - g:ia';
  }

  return format_date($node->created, 'custom', $format);
}*/