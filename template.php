<?php
/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('ngp_left_span'))) {
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(
    'ngp_left_span' => 4,
    'ngp_left_prepend' => 1,
    'ngp_left_append' => 1,
    'ngp_left_top' => 1,
    'ngp_left_bottom' => 1,
    
    'ngp_right_span' => 5,
    'ngp_right_prepend' => 1,
    'ngp_right_append' => 1,
    'ngp_right_top' => 1,
    'ngp_right_bottom' => 1,
    
    'ngp_center_prepend' => 1,
    'ngp_center_append' => 1,
    'ngp_center_top' => 1,
    'ngp_center_bottom' => 1,
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

/**
 * Intercept page template variables
 *
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */
function ngp_preprocess_page(&$vars) {
  global $user;
  $vars['path'] = base_path() . path_to_theme() .'/';
  $vars['user'] = $user;
  
  if (module_exists('path')) {
    $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q']));
    if ($alias != $_GET['q']) {
      $template_filename = 'page';
      foreach (explode('/', $alias) as $path_part) {
        $template_filename = $template_filename . '-' . $path_part;
        $vars['template_files'][] = $template_filename;
      }
    }
  }
  $vars['body_classes'] .= implode(' ', $vars['template_files']);

  // Fixup the $head_title and $title vars to display better.
  $title = drupal_get_title();
  $headers = drupal_set_header();

  // wrap taxonomy listing pages in quotes and prefix with topic
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $title = t('Topic') .' &#8220;'. $title .'&#8221;';
  }
  // if this is a 403 and they aren't logged in, tell them they need to log in
  else if (strpos($headers, 'HTTP/1.1 403 Forbidden') && !$user->uid) {
    $title = t('Please login to continue');
  }
  $vars['title'] = $title;

  if (!drupal_is_front_page()) {
    $vars['head_title'] = $title .' | '. $vars['site_name'];
    if ($vars['site_slogan'] != '') {
      $vars['head_title'] .= ' &ndash; '. $vars['site_slogan'];
    }
  }

  $vars['meta'] = '';
  if (!module_exists('nodewords')) {
    // SEO optimization, add in the node's teaser, or if on the homepage, the mission statement
    // as a description of the page that appears in search engines
    if ($vars['is_front'] && $vars['mission'] != '') {
      $vars['meta'] .= '<meta name="description" content="'. ngp_trim_text($vars['mission']) .'" />'."\n";
    }
    else if (isset($vars['node']->teaser) && $vars['node']->teaser != '') {
      $vars['meta'] .= '<meta name="description" content="'. ngp_trim_text($vars['node']->teaser) .'" />'."\n";
    }
    else if (isset($vars['node']->body) && $vars['node']->body != '') {
      $vars['meta'] .= '<meta name="description" content="'. ngp_trim_text($vars['node']->body) .'" />'."\n";
    }
    // SEO optimization, if the node has tags, use these as keywords for the page
    if (isset($vars['node']->taxonomy)) {
      $keywords = array();
      foreach ($vars['node']->taxonomy as $term) {
        $keywords[] = $term->name;
      }
      $vars['meta'] .= '<meta name="keywords" content="'. implode(',', $keywords) .'" />'."\n";
    }
  
    // SEO optimization, avoid duplicate titles in search indexes for pager pages
    if (isset($_GET['page']) || isset($_GET['sort'])) {
      $vars['meta'] .= '<meta name="robots" content="noindex,follow" />'. "\n";
    }
  }

  $left_span = theme_get_setting('ngp_left_span');
  $left_prepend = theme_get_setting('ngp_left_prepend');
  $left_append = theme_get_setting('ngp_left_append');
  $left_top = theme_get_setting('ngp_left_top');
  $left_bottom = theme_get_setting('ngp_left_bottom');
  $right_span = theme_get_setting('ngp_right_span');
  $right_prepend = theme_get_setting('ngp_right_prepend');
  $right_append = theme_get_setting('ngp_right_append');
  $right_top = theme_get_setting('ngp_right_top');
  $right_bottom = theme_get_setting('ngp_right_bottom');
  $center_prepend = theme_get_setting('ngp_center_prepend');
  $center_append = theme_get_setting('ngp_center_append');
  $center_top = theme_get_setting('ngp_center_top');
  $center_bottom = theme_get_setting('ngp_center_bottom');
  
  // determine layout
  $vars['left_classes'] = 'col-left span-'.$left_span;
  $vars['left_classes'] .= ($left_prepend != '0') ? ' prepend-'.$left_prepend : '';
  $vars['left_classes'] .= ($left_append != '0') ? ' append-'.$left_append : '';
  $vars['left_classes'] .= ($left_top == 1) ? ' prepend-top' : '';
  $vars['left_classes'] .= ($left_bottom == 1) ? ' append-bottom' : '';
  $vars['right_classes'] = 'col-right span-'.$right_span;
  $vars['right_classes'] .= ($right_prepend != '0') ? ' prepend-'.$right_prepend : '';
  $vars['right_classes'] .= ($right_append != '0') ? ' append-'.$right_append : '';
  $vars['right_classes'] .= ($right_top == 1) ? ' prepend-top' : '';
  $vars['right_classes'] .= ($right_bottom == 1) ? ' append-bottom' : '';
  $vars['center_classes'] = 'col-center';
  $vars['center_classes'] .= ($center_prepend != '0') ? ' prepend-'. $center_prepend : '';
  $vars['center_classes'] .= ($center_append != '0') ? ' append-'. $center_append : '';
  $vars['center_classes'] .= ($center_top == 1) ? ' prepend-top' : '';
  $vars['center_classes'] .= ($center_bottom == 1) ? ' append-bottom' : '';
  
  // 3 columns
  if ($vars['layout'] == 'both') {
    $span = 24 - ($left_span + $left_prepend + $left_append + $right_span + $right_prepend + $right_append + $center_prepend + $center_append);
    $vars['center_classes'] .= ' span-'. $span .' col-3';
    $vars['right_classes'] .= ' last';
  }
  // 2 columns - left and center
  else if ($vars['layout'] == 'left') {
    $span = 24 - ($left_span + $left_prepend + $left_append + $center_prepend + $center_append);
    $vars['center_classes'] .= ' span-'. $span .' col-2 last';
  }
  // 2 columns - right and center
  else if ($vars['layout'] == 'right') {
    $span = 24 - ($right_span + $right_prepend + $right_append + $center_prepend + $center_append);
    $vars['center_classes'] .= ' span-'. $span .' col-2';
    $vars['right_classes'] .= ' last';
  }
  // 1 column - center
  else {
    $span = 24 - ($center_prepend + $center_append);
    $vars['center_classes'] .= ' span-'. $span .' col-1 last';
  }
}

function phptemplate_crmngp_ui_form_subscribe($form) {
  if (file_exists(path_to_theme() . '/images/subscribe.gif')) {
    $form['subscribe_submit']['#type'] = 'image_button';
    $form['subscribe_submit']['#src'] = base_path() . path_to_theme() . '/images/subscribe.gif';
    $form['subscribe_submit']['#attributes'] = array('alt' => t('Subscribe'));
  }
  return drupal_render($form);
}

function phptemplate_crmngp_ui_form_contribute($form) {
  if (file_exists(path_to_theme() . '/images/contribute.gif')) {
    $form['contribute_submit']['#type'] = 'image_button';
    $form['contribute_submit']['#src'] = base_path() . path_to_theme() . '/images/contribute.gif';
    $form['contribute_submit']['#attributes'] = array('alt' => t('Contribute'));
  }
  return drupal_render($form);
}

function ngp_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }
  //dsm($link);
  
  $link['localized_options']['attributes'] = array('class' => ngp_id_safe($link['href']), 'id' => 'mlid-' . $link['mlid']);

  return l($link['title'], $link['href'], $link['localized_options']);
}


/**    
 * Converts a string to a suitable html ID attribute.
 * 
 *  <a href="http://www.w3.org/TR/html4/struct/global.html#h-7.5.2" title="http://www.w3.org/TR/html4/struct/global.html#h-7.5.2" rel="nofollow">http://www.w3.org/TR/html4/struct/global.html#h-7.5.2</a> specifies what makes a
 *  valid ID attribute in HTML. This function:
 * 
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 * 
 * @param $string
 *   The string
 * @return
 *   The converted string
 */    
function ngp_id_safe($string) {
  //dsm($string);
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Trim a post to a certain number of characters, removing all HTML.
 */
function ngp_trim_text($text, $length = 150) {
  // remove any HTML or line breaks so these don't appear in the text
  $text = trim(str_replace(array("\n", "\r"), ' ', strip_tags($text)));
  $text = trim(substr($text, 0, $length));
  $lastchar = substr($text, -1, 1);
  // check to see if the last character in the title is a non-alphanumeric character, except for ? or !
  // if it is strip it off so you don't get strange looking titles
  if (preg_match('/[^0-9A-Za-z\!\?]/', $lastchar)) {
    $text = substr($text, 0, -1);
  }
  // ? and ! are ok to end a title with since they make sense
  if ($lastchar != '!' && $lastchar != '?') {
    $text .= '...';
  }
  return $text;
}

function ngp_node_submitted($node) {
  //dsm($node);
  if (empty($node->body)) {
    $format = '<\s\t\ro\n\g>M</\s\t\ro\n\g><\e\m>j</\e\m>';
  }
  else {
    $format = 'F j, Y - g:ia';
  }

  return format_date($node->created, 'custom', $format);
}

function ngp_links($links, $attributes = array('class' => 'links')) {
  if (!empty($links['blog_usernames_blog'])) {
    $links['blog_usernames_blog']['href'] = 'blog';
    $links['blog_usernames_blog']['title'] = 'Our blog';
  }
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

function ngp_footer_links($links, $attributes = array('class' => 'links')) {
  if (!empty($links['blog_usernames_blog'])) {
    $links['blog_usernames_blog']['href'] = 'blog';
    $links['blog_usernames_blog']['title'] = 'Our blog';
  }
  global $language;
  $output = '';

  if (count($links) > 0) {
    $output = '<div'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }
      if ($i != $num_links) {
        $output .= ' | ';
      }

      $i++;
    }

    $output .= '</div>';
  }

  return $output;
}


/**
 * @defgroup "Theme Callbacks"
 * @{
 * @see imagefield_theme().
 */
function ngp_imagefield_image($file, $alt = '', $title = '', $attributes = NULL, $getsize = TRUE) {
  //dsm(get_defined_vars());
  $file = (array)$file;
  if (!is_file($file['filepath'])) {
    return '<!-- File not found: '. $file['filepath'] .' -->';
  }

  if ($getsize) {
    // Use cached width and height if available.
    if (!empty($file['data']['width']) && !empty($file['data']['height'])) {
      $attributes['width']  = $file['data']['width'];
      $attributes['height'] = $file['data']['height'];
    }
    // Otherwise pull the width and height from the file.
    elseif (list($width, $height, $type, $image_attributes) = @getimagesize($file['filepath'])) {
      $attributes['width'] = $width;
      $attributes['height'] = $height;
    }
  }

  if (!empty($title)) {
    $attributes['title'] = $title;
  }

  // Alt text should be added even if it is an empty string.
  $attributes['alt'] = $alt;

  // Add a timestamp to the URL to ensure it is immediately updated after editing.
  $query_string = '';
  if (isset($file['timestamp'])) {
    $query_character = (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PRIVATE && variable_get('clean_url', '0') == '0') ? '&' : '?';
    $query_string = $query_character . $file['timestamp'];
  }

  $url = $GLOBALS['base_url'] .'/'. $file['filepath'];
  $attributes['src'] = $url;
  $attributes = drupal_attributes($attributes);
  return '<img '. $attributes .' />';
}