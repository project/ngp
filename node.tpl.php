<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php print ' node-type-'.$type; ?><?php if ($teaser) { print ' node-teaser'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

<?php if (!empty($field_image) && !empty($field_image[0]) && !empty($field_image[0]['view'])): ?>
<div class="field-image"><?php print $field_image[0]['view']; ?></div>
<div class="node-body">
<?php endif; ?>

  <?php
    //dsm(get_defined_vars());
    if ($page == 0) {
      print '<div class="title clearfix">';
      if ($node->type == 'event') {
        print '<div class="submitted">'. $node->field_date[0]['view'] .'</div>';
      }
      else if ($submitted) {
        print '<div class="submitted">'. $submitted .'</div>';
      }
      print '<h2><a href="'. $node_url .'" title="'. $title .'">'. $title .'</a></h2>';
      print '</div>';
    }
    else if ($submitted) {
      print '<div class="submitted">'. $submitted .'</div>';
    }
  ?>

  <div class="content clear-block">
    <?php print $picture ?>
    <?php print $content ?>
  </div>

  <?php if ($terms && ($node->type != 'page')): ?>
    <div class="terms"><?php print $terms ?></div>
  <?php endif;?>

<?php
  if ($links) {
    print '<div class="node-links">'. $links .'</div>';
  }
?>

<?php if (!empty($field_image) && !empty($field_image[0]) && !empty($field_image[0]['view'])): ?>
</div>
<?php endif; ?>

</div>